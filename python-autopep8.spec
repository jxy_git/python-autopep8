%global _empty_manifest_terminate_build 0
Name:		python-autopep8
Version:	2.3.1
Release:	1
Summary:	A tool that automatically formats Python code to conform to the PEP 8 style guide
License:	MIT
URL:		https://github.com/hhatto/autopep8
Source0:	https://files.pythonhosted.org/packages/source/a/autopep8/autopep8-%{version}.tar.gz
BuildArch:	noarch


%description
A tool that automatically formats Python code to conform to the PEP 8 style guide

%package -n python3-autopep8
Summary:	A tool that automatically formats Python code to conform to the PEP 8 style guide
Provides:	python-autopep8 = %{version}-%{release}
BuildRequires:	python3-devel
BuildRequires:	python3-pip
BuildRequires:	python3-wheel
BuildRequires:	python3-flit
Requires:	python3-pycodestyle
Requires:	python3-toml
%description -n python3-autopep8
A tool that automatically formats Python code to conform to the PEP 8 style guide

%package help
Summary:	Development documents and examples for autopep8
Provides:	python3-autopep8-doc
%description help
A tool that automatically formats Python code to conform to the PEP 8 style guide

%prep
%autosetup -n autopep8-%{version}

%build
%pyproject_build

%install
%pyproject_install
install -d -m755 %{buildroot}/%{_pkgdocdir}
if [ -d doc ]; then cp -arf doc %{buildroot}/%{_pkgdocdir}; fi
if [ -d docs ]; then cp -arf docs %{buildroot}/%{_pkgdocdir}; fi
if [ -d example ]; then cp -arf example %{buildroot}/%{_pkgdocdir}; fi

%files -n python3-autopep8
%{python3_sitelib}/*
%{_bindir}/autopep8

%files help
%{_docdir}/*

%changelog
* Thu Oct 17 2024 jiangxinyu <jiangxinyu@kylinos.cn> - 2.3.1-1
- Update package to version 2.3.1
- Fix argument parser errors are printed without a trailing newline
  skip e501 fixed method for f-string line without aggressive option

* Wed Nov 15 2023 jiangxinyu <jiangxinyu@kylinos.cn> - 2.0.4-1
- Update package to version 2.0.4

* Wed Jun 28 2023 sunhui <sunhui@kylinos.cn> - 2.0.2-1
- Update package to 2.0.2 

* Sun Jun 25 2023 liukuo <liukuo@kylinos.cn> - 1.5.7-2
- License compliance rectification

* Fri Jul 23 2021 Xu Jin <jinxu@kylinos.cn> - 1.5.7-1
- Update package to 1.5.7

* Sat Jul 18 2020 Python_Bot <Python_Bot@openeuler.org> - 1.5.3-1
- Package Spec generated
